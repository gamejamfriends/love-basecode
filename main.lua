local const = require './const'

local anim8 = require './lib/anim8'

local debugGraph = require './lib/debugGraph'
local fpsGraph
local memGraph
local showDebug = false

local mainCanvas = love.graphics.newCanvas(const.mainCanvasW, const.mainCanvasH)

-- TODO: Delete this placeholder
local placeholder = love.graphics.newImage("img/placeholder.png")
local placeholderSprite
local placeholderAnim

function love.load()
   love.window.setMode(const.mainCanvasW*4, const.mainCanvasH*4, {
      resizable = true,
      minwidth = const.mainCanvasW,
      minheight = const.mainCanvasH,
   })
   love.graphics.setBackgroundColor(0, 0, 0)
   mainCanvas:setFilter("nearest", "nearest", 0)

   fpsGraph = debugGraph:new('fps', 0, 0)
   memGraph = debugGraph:new('mem', 0, 30)

   -- TODO: Remove
   placeholderSprite = love.graphics.newImage('img/placeholder-sprite.png')
   local g = anim8.newGrid(
      32,
      32,
      placeholderSprite:getWidth(),
      placeholderSprite:getHeight()
   )
   placeholderAnim = anim8.newAnimation(g('1-8','1-4'), 0.1)
end

function love.update(dt)
   fpsGraph:update(dt)
   memGraph:update(dt)

   -- TODO: Insert main rendering here
   placeholderAnim:update(dt)
end

function love.draw()
   love.graphics.setCanvas(mainCanvas)
   -- TODO: Insert main rendering here
   love.graphics.draw(placeholder)
   placeholderAnim:draw(placeholderSprite, 64, 100)
   --
   love.graphics.setCanvas()

   -- blit mainCanvas to screen with scaling

   local windowW, windowH = love.graphics.getDimensions()
   local mainCanvasScale = math.floor(math.min(
      windowW / const.mainCanvasW,
      windowH / const.mainCanvasH
   ))

   love.graphics.draw(
      mainCanvas,
      (windowW - const.mainCanvasW*mainCanvasScale) / 2,
      (windowH - const.mainCanvasH*mainCanvasScale) / 2,
      0,
      mainCanvasScale,
      mainCanvasScale
   )

   if showDebug then
      fpsGraph:draw()
      memGraph:draw()
   end
end

function love.keypressed(key)
  if key == 'f1' then
     showDebug = not showDebug
  end
end
